package com.oga.springboot.app.oauth.services;

import com.oga.springboot.app.usuarios.commons.models.entity.Usuario;

public interface IUsuarioService {
	
	public Usuario findByUsername(String username);

}
