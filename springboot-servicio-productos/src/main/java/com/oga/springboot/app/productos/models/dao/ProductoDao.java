package com.oga.springboot.app.productos.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.oga.springboot.app.productos.models.entity.Producto;

/**
 * The Interface ProductoDao.
 * here we use CrudRepository which is a spring component, that's why it can be
 * injected without any annotation: @component, @repository etc..
 * because its registration is made automatically
 */
public interface ProductoDao extends CrudRepository<Producto, Long>{

}
