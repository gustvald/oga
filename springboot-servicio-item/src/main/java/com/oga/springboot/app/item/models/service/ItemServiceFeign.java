package com.oga.springboot.app.item.models.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.oga.springboot.app.item.clientes.ProductoClienteRest;
import com.oga.springboot.app.item.models.Item;
import com.oga.springboot.app.item.models.Producto;

/**
 * The Class ItemServiceFeign.
 * this implementation is @Primary, it means that this will be used instead the other
 * in restTemplate
 * this is a better option for a service client included in Spring Cloud Netflix environment
 * easier to use and Ribbon (load balance) included 
 */
@Service
@Primary
public class ItemServiceFeign implements ItemService {

	/** The cliente feign. */
	@Autowired
	private ProductoClienteRest clienteFeign;
	
	/* (non-Javadoc)
	 * @see com.oga.springboot.app.item.models.service.ItemService#findAll()
	 */
	@Override
	public List<Item> findAll() {
		return clienteFeign.listar().stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
	}

	/* (non-Javadoc)
	 * @see com.oga.springboot.app.item.models.service.ItemService#findById(java.lang.Long, java.lang.Integer)
	 */
	@Override
	public Item findById(Long id, Integer cantidad) {
		return new Item(clienteFeign.detalle(id), cantidad);
	}

	@Override
	public Producto save(Producto producto) {
		return clienteFeign.crear(producto);
	}

	@Override
	public Producto update(Producto producto, Long id) {
		return clienteFeign.update(producto, id);
	}

	@Override
	public void delete(Long id) {
		clienteFeign.eliminar(id);
	}

}
